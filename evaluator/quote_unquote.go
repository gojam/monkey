package evaluator

import (
	"gitlab.com/gojam/monkey/ast"
	"gitlab.com/gojam/monkey/object"
)

func quote(node ast.Node) object.Object {
	return &object.Quote{Node: node}
}
